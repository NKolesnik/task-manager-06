package ru.t1consulting.nkolesnik.tm;

import ru.t1consulting.nkolesnik.tm.constant.ArgumentConst;
import ru.t1consulting.nkolesnik.tm.constant.TerminalConst;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgument(args)) {
            System.exit(0);
        }
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) {
            return;
        }
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) {
            return false;
        }
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void close() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikolay Kolesnik");
        System.out.println("E-mail: kolesnik.nik.vrn@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show developer info.\n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show application version.\n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show terminal commands.\n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application.\n", TerminalConst.EXIT);
    }

    public static void showErrorArgument(final String arg) {
        System.err.printf("Error! This argument `%s` is not supported\n", arg);
    }

    public static void showErrorCommand(final String arg) {
        System.err.printf("Error! This command `%s` is not supported\n", arg);
    }

}
